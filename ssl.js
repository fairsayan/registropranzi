var fs = require('fs');
var http = require('http');
var https = require('https');
var confs = require('./confs.js');

module.exports = function (app) {

  if (process.env.DEV){
    http.createServer(app).listen(confs.server.port, confs.server.ip);
  } else {
    var credentials = {
      key: fs.readFileSync(confs.ssl.key, 'utf8'),
      cert: fs.readFileSync(confs.ssl.certificate, 'utf8')
    };
    https.createServer(credentials, app).listen(confs.ssl.port, confs.server.ip);
  }

};
