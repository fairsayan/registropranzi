var nodemailer = require('nodemailer');
var mg = require('nodemailer-mailgun-transport');

var auth = {
  auth: {
    api_key: 'key-0ad525db4670fa7af4013b1fb27b42e0',
    domain: 'sandbox52b445f87fc747c6a3fbc7b68d1ba818.mailgun.org'
  }
}

var mailer = nodemailer.createTransport(mg(auth));

function sendSummary(data){
  var resoconto = genSummary(data);
  db.q(null,null,"select * from summary_mail",null,function(req, res, err, rows, fields){
    for (var i in rows){
      mailer.sendMail({from:"RegistroPranzi@elis.org", to:rows[i].email, subject:"Resoconto", text:resoconto});
    }
  });
}

function genSummary(data){
  var resoconto = {
    colazione: { sacchetti: []},
    pranzo: {totale:0, diete:[], sacchetti:[]},
    cena: {totale:0, diete:[], sacchetti:[]}
  };
  for (var i in data){ var d = data[i];
    if (d.period=="lunch" && d.presence=="yes" && d.total) { resoconto.pranzo.totale = d.total; resoconto.timestamp = d.timestam}
    if (d.period=="dinner" && d.presence=="yes" && d.total) resoconto.cena.totale = d.total;
    if (d.period="breakfast" && d.presence=="pocket" && d.pocket_id) resoconto.colazione.sacchetti.push(d.pocket_id);
    if (d.period="lunch" && d.presence=="pocket" && d.pocket_id) resoconto.pranzo.sacchetti.push(d.pocket_id);
    if (d.period="dinner" && d.presence=="pocket" && d.pocket_id) resoconto.cena.sacchetti.push(d.pocket_id);
    if (d.period="lunch" && d.presence=="yes" && d.diet_id) resoconto.pranzo.diete.push(d.diet_id);
    if (d.period="dinner" && d.presence=="yes" && d.diet_id) resoconto.cena.diete.push(d.diet_id);
  }
  resoconto.colazione.totale_sacchetti = resoconto.colazione.sacchetti.length;
  resoconto.pranzo.totale_sacchetti = resoconto.pranzo.sacchetti.length;
  resoconto.cena.totale_sacchetti = resoconto.cena.sacchetti.length;
  resoconto.colazione.sacchetti = resoconto.colazione.sacchetti.join(",");
  resoconto.pranzo.sacchetti = resoconto.pranzo.sacchetti.join(",");
  resoconto.cena.sacchetti = resoconto.cena.sacchetti.join(",");
  resoconto.pranzo.diete = resoconto.pranzo.diete.join(",");
  resoconto.cena.diete = resoconto.cena.diete.join(",");
  return JSON.stringify(resoconto,null,4);
}

function getSummary(timestamp, callback){
  	db.q(null, null, cacheFile.files["summary.sql"],
  		[timestamp,timestamp,timestamp,timestamp,timestamp,timestamp],
  		function(req, res, err, rows, fields){
  			if (err) console.log(err);
  			callback(rows);
  		}
  	);
}

var schedule = require('node-schedule');
var moment = require('moment'); var tz = require("moment-timezone");
var cacheFile = require('./cacheFile'); cacheFile.add("summary.sql");

var db;

module.exports.start = function(db_conn){
  db = db_conn;
  try { var jobs = schedule.scheduleJobs;
  for (var i in jobs) schedule.cancelJob(jobs[i]) } catch(e){};
  var summaryJob = schedule.scheduleJob({hour:18}, function(){ //time based on server location (use later.js)
      getSummary(moment.tz("Europe/Rome").add(1,"day").startOf("day").valueOf(),sendSummary);
  });
  return {summaryJob:summaryJob};
}
