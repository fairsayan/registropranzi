var jsonschema = require('jsonschema');
var debug = require('debug')('validation');
var moment = require('moment')
var timezone = require('moment-timezone')

//presences validation schema
var presence_schema = { "type": "array", // [{"id":113,"timestamp":86400001,"period":"dinner","presence":"yes"},
	"items": {"type": "object", "properties": {
		"id": {"type":"integer", "min": 1, "max":1000},
		"timestam": {"type": "integer", "min":1},
		"period": {"type": "string"},
		"presence": {"type": "string"},
	}, "required":["id", "timestam", "period", "presence"]}
};

//validate one entry
function validatePresence(presence, date){
	if (presence.id<0 && presence.id>10000) {
		debug("id");
		return {"presence.id<0 && presence.id>1000": presence.id}
	};
	if (moment.tz(presence.timestam,"Europe/Rome").valueOf()!=moment.tz(presence.timestam,"Europe/Rome").startOf("day").valueOf()) {
		debug("startOf");
		return {'moment.tz(presence.timestam,"Europe/Rome").valueOf()!=moment.tz(presence.timestam,"Europe/Rome").startOf("day").valueOf()':
			moment.tz(presence.timestam,"Europe/Rome").valueOf()+"!="+moment.tz(presence.timestam,"Europe/Rome").startOf("day").valueOf()
		}
	}
	if ((moment.tz(presence.timestam,"Europe/Rome").startOf("day").valueOf()-3600000)<date){
		debug("23:00");
		return {'(moment.tz(presence.timestam,"Europe/Rome").startOf("day").valueOf()-3600000)<date':
			(moment.tz(presence.timestam,"Europe/Rome").startOf("day").valueOf()-3600000)+"<"+date
		}
	}
	if (["lunch", "dinner", "breakfast"].indexOf(presence.period)<0) {
		debug("period");
		return {'["lunch", "dinner", "breakfast"].indexOf(presence.period)<0': presence.period}
	}
	if (["yes", "no", "pocket", "yes_pocket"].indexOf(presence.presence)<0) {
		debug("presence");
		return {'["yes", "no", "pocket", "yes_pocket"].indexOf(presence.presence)<0': presence.presence}
	}
	if (presence.period=="breakfast" && ((presence.timestam-90000000)<date))
		return {'presence.period=="breakfast" && ((presence.timestam-90000000)<date)': (presence.timestam-90000000)
	}
	if (moment.tz(presence.timestam,"Europe/Rome").day()== 0 /*domenica*/ &&
		moment.tz(presence.timestam,"Europe/Rome").startOf('day').add(-2,'days').add(23,'hours') < date ) {
			return {'sabato per domenica': ''}
	}
}

function validatePresences(presences){
	var validity = jsonschema.validate(presences, presence_schema);
	validity.customErrors = [];
	var date = new Date().valueOf();
	for (var i in presences){
		var customError = validatePresence(presences[i],date);
		if (customError) validity.customErrors.push(customError);
	}
	return validity;
}

module.exports = validatePresences;
