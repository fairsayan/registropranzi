(
	select timestam, period, presence, count(presence) as total, null as diet_id, null as pocket_id
	from (
		select timestam, period, presence, bid as id, name, diet
		from (
			select aid as bid, timestam, period, presence, name, diet
			from (	
				(select id as aid, timestam, period, 'yes' as presence from presences where presence = 'yes_pocket') union
				(select id as aid, timestam, period, 'pocket' as presence from presences where presence = 'yes_pocket') union
				(select id as aid, timestam, period, presence from presences)
			) ttpresences join alumni on ttpresences.aid = alumni.id
		) as tpresences
		where
			(period <> 'breakfast' and timestam = ?) or
			(period = 'breakfast' and timestam = (? + 86400000))
    ) as t1
	group by presence, period, timestam
)
union
(
	select timestam, period, presence, null as total, id as diet_id, null as pocket_id
	from (
		select timestam, period, presence, bid as id, name, diet
		from (
			select aid as bid, timestam, period, presence, name, diet
			from (	
				(select id as aid, timestam, period, 'yes' as presence from presences where presence = 'yes_pocket') union
				(select id as aid, timestam, period, 'pocket' as presence from presences where presence = 'yes_pocket') union
				(select id as aid, timestam, period, presence from presences)
			) ttpresences join alumni on ttpresences.aid = alumni.id
		) as tpresences
		where
			(period <> 'breakfast' and timestam = ?) or
			(period = 'breakfast' and timestam = (? + 86400000))
    ) as t2
	where diet <> 'no' and presence = 'yes'
)
union
(
	select timestam, period, presence, null as total, null as diet_id, id as pocket_id
	from (
		select timestam, period, presence, bid as id, name, diet
		from (
			select aid as bid, timestam, period, presence, name, diet
			from (	
				(select id as aid, timestam, period, 'yes' as presence from presences where presence = 'yes_pocket') union
				(select id as aid, timestam, period, 'pocket' as presence from presences where presence = 'yes_pocket') union
				(select id as aid, timestam, period, presence from presences)
			) ttpresences join alumni on ttpresences.aid = alumni.id
		) as tpresences
		where
			(period <> 'breakfast' and timestam = ?) or
			(period = 'breakfast' and timestam = (? + 86400000))
    ) as t3
    where presence = 'pocket'
)