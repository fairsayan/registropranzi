var express = require('express');
var confs = require('./confs.js');

var xAdmin = require('express-admin');

var config = {
    dpath: process.env.DEV?'./express-admin-config/':confs.root_path+'/express-admin-config/',
    config: require('./express-admin-config/config.json'),
    settings: require('./express-admin-config/settings.json'),
    custom: require('./express-admin-config/custom.json'),
    users: require('./express-admin-config/users.json')
};

module.exports = function (app){
  xAdmin.init(config, function (err, admin) {
      if (err) return console.log(err);
      app.use('/admin', express.static(__dirname + "/node_modules/express-admin-static"));
      app.use('/admin', admin);
  });
}
