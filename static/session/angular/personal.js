angular.module("registropranzi")
  .controller("personal", ["$scope","$http",function($scope, $http){
    $http.get("/rest/user/113/presence?from_timestamp=1447023600000&to_timestamp=1447542000000")
      .then(function(response){

        var dates = {
          from: moment().startOf('isoweek').add(0,'weeks').startOf('day').valueOf(),
          to: moment().endOf('isoweek').add(0,'weeks').startOf('day').valueOf(),
          range: []
        }
        for (var now = moment(dates.from); now>=dates.from && now<=dates.to; now.add(1,'days')){
          dates.range.push(moment(now.valueOf()));
        }

        var built = _.chain(response.data)
          .groupBy("timestam")
          .mapValues(function (day){
            return _.indexBy(day, 'period')
          }).value();

        $scope.presences = _.map(dates.range, function(date){
          date.presences = built[date.valueOf()];
          return date;
        })

      })
  }]);
