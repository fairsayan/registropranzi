var fs = require('fs');
var debug = require('debug')('cacheFile');

//cached files
var files = {};

function readFile(path, callback){
	fs.readFile(path, 'utf8', function(err,data){
		if (!err) {
			files[path] = data;
			if(callback) callback(files[path])
		} else debug(err);
	});
}

//read a file, callback() onchanges
function cacheFile(path, callback){
	readFile(path,callback);
	fs.watchFile(path, function(ev,fname){
		readFile(path, callback);
	});
};

module.exports.add = cacheFile;
module.exports.files = files;
