var express = require('express');
var db;
var session = require('express-session');
var debug = require('debug')('login');

var router = express.Router();

router.use(session({
    store: new require('session-file-store')(session)({}),
    secret: 'keyboard cat',
    resave: false,
    saveUninitialized: true
}));

router.use('/', express.static(__dirname + '/static/no_session'));

router.post('/login',
  require('body-parser').urlencoded({extended: true}),
  login);
router.post('/rest/login',
  require('body-parser').urlencoded({extended: true}),
  function (req, res, next){req.rest=true; next()},
  login);

function login(req, res) {
	try {
		var username = req.body.username;
		var password = req.body.password;
	} catch (e) {return res.redirect("/")};
  debug(password);
	db.q(req,res,"select username, password, permission from users where username=? and password=md5(?)",[username,password],function(req,res,err,rows,fields){
    req.session.user = rows[0];
		if (req.rest) res.json({"ok":"ok"});
    else res.redirect("/");
	});
}

router.post('/change', require('body-parser').urlencoded({extended: true}), function(req, res){
  try {	var password = req.body.password } catch (e) {return res.redirect("/")};
  debug(password);
  db.q(req,res,"update users set password=md5(?) where username=? and password=?",
    [password, req.session.user.username, req.session.user.password],
    function(req,res,err,rows,fields){
      res.redirect("/");
    });
});

router.get('/logout', function(req, res){
	req.session.destroy();
	res.end();
});

router.get('/',function(req,res){
	if (!req.session.user) return res.redirect("/login.html");
	res.redirect("/index.html");
});

router.use('/', function checkUser(req, res, next){
	if (req.session.user) return next();
	res.redirect("/")
});

module.exports = function(db_conn){
	db = db_conn;
	return router;
}
