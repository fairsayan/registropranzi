var rp = (function(){ var exp = {};

var lastAction = new Date();
function guiLock(callback){ return function(){
  var now = new Date();
  if (now - lastAction < 1000) lastAction = now;
  else setTimeout(callback, 500, lastAction = now);
}}

var peopleCache, presencesCache, rangeCache;
function loadTableAll(id, callback){
  loadingStart();
  $.getJSON("/rest/users/list", function(people){ peopleCache=people;
    var range = dateRange.get(); rangeCache = range;
    $.getJSON("/rest/users/presence", range, function (presences){ presencesCache=presences;
      $(".content.all > div").empty();
       var table = genTableAll(sortPeople(people), range, presences);
      $(".content.all > div").replaceWith(table);
      $(".content.personal").addClass("none");
      $(".content.all").removeClass("none");
      loadingEnd(); if (callback) callback();
      //$("meta").replaceWith($("<meta>",{name:"viewport", content:"user-scalable=yes"}));
    });
  });
}

function loadtablePersonal(id, callback){
  var range = dateRange.get();
  $.getJSON("/rest/user/"+id+"/presence", range, function (presences){
    $(".content.all > div").empty();
    var table = genTablePersonal(id, range, presences);
    $("#"+id).empty().append(table);
    $(".content.all").addClass("none");
    $(".content.personal").removeClass("none");
    if (callback) callback();
  });
}

function showSummary(){ var timestamp;
  $.getJSON("/rest/users/summary",{timestamp:(timestamp=$(this).data("timestamp"))},function (data){
    $("#overshadow").addClass("on").load("summary.html", function(){
      summary(data, timestamp);
      $(".bare").click(guiLock(function(){bareSummary(data); return false}));
      history.pushState({},"","");
      $(".done").click(guiLock(function(){history.back()}));
      $("#summary-names").click(function(){$(".summary-names").toggleClass("none")});
    });
  });
}

function nextWeek(){
  history.pushState({callback:"table", args:{viewing_week:(++dateRange.viewing_week), loadTable:loadTable}},"","");
  loadtable[loadTable](id);
  if(loadTable=="personal") loadPersonalTables();
  $.cookie("viewing_week",dateRange.viewing_week);
}

function lastWeek(){
  history.pushState({callback:"table", args:{viewing_week:(--dateRange.viewing_week), loadTable:loadTable}},"","");
  loadtable[loadTable](id);
  if(loadTable=="personal") loadPersonalTables();
  $.cookie("viewing_week",dateRange.viewing_week);
}

function switchView(){
  if (loadTable=="all") $.cookie("tabletype", loadTable = "personal");
  else if (loadTable=="personal") $.cookie("tabletype", loadTable = "all");
  searchCache = null;
  history.pushState({callback:"table", args:{viewing_week:(dateRange.viewing_week), loadTable:loadTable}},"","");
  loadtable[loadTable](id, function(){
    if (loadTable=="all") {
      $("input#searchbox").removeClass("shrink");
      $("#overflow-cover").addClass("on");
    }
    if (loadTable=="personal") {
      $("input#searchbox").addClass("shrink");
      $("#overflow-cover").removeClass("on");
      genPersonalTables();
      loadPersonalTables();
      //$("meta").replaceWith($("<meta>",{name:"viewport", content:"initial-scale=1.0, maximum-scale=1.0, user-scalable=no"}));
    }
  });
}

function help(){
  $("#overshadow").addClass("on").load("help.html", function(){
    $("#logout").click(guiLock(logout));
    $("#changeId").click(guiLock(changeId));
    $("#got").click(guiLock(function(){history.back()}));
    $.get("admin").then(function(){$("#amministra").css({display:"inline-block"})});
  });
  history.pushState({},"","");
}

var searchCache;
function search(e){
  var string = $(e.target).val().toString().toLowerCase();
  (searchCache||(searchCache=$(".list > .item:not(.head)")))
  .each(function(i,v){ v = $(v);
    var match = v.find(".id").text() +" "+ v.find(".name").text();
    if (match.toLowerCase().indexOf(string) >= 0)
      v.css({display:"table-row"})
    else
      v.css({display:"none"})
  });
}

function logout(){
  $.get("/logout",function(){
    location = location.origin;
  });
}

function changeId(){
  document.cookie="id=;";
  location = location.origin;
}

var spinnerDelay;

function loadingStart(){
  spinnerDelay = setTimeout(function(){
    $("#overshadow").addClass("on").empty()
      .append($("<div>",{class:"fa fa-spin fa-cog fa-2x"}));
  },500);
}

function loadingEnd(){
  clearTimeout(spinnerDelay);
  setTimeout(function(){
    $("#overshadow").removeClass("on").empty();
  },500);
}

$.cookie = function (key, value){
  if (value) document.cookie = key+"="+value;
  try {return new RegExp(key+"(?:=[0-9a-zA-Z,]*)").exec(document.cookie)[0].split("=")[1];} catch(e){};
}

// history manipulation

var loadTable = $.cookie("tabletype") || "personal";

var historyStates = {};

var loadtable = {personal:loadtablePersonal, all:loadTableAll};

window.addEventListener("popstate",function(e){historyStates[e.state.callback](e.state.args)});

historyStates.table = function (args){
  dateRange.viewing_week = args.viewing_week;
  loadtable[args.loadTable](id);
  $("#overshadow").removeClass("on");
}

// login

var id;

$(document).ready(function(){
  summary();
  if ($(".summary").length==1) return;
  $.get("navbar.html", function(data){
    $("body").append(data);
    $("#switchView").click(guiLock(switchView));
    $("#nextWeek").click(guiLock(nextWeek));
    $("#lastWeek").click(guiLock(lastWeek));
    $("#help").click(guiLock(help));
    $("#flush").click(flush);
    $("#today").click(function (){dateRange.viewing_week=-1; guiLock(nextWeek)()});
    $("#one_all").click(function(){dateRange.viewing_week=-1;dateRange.daysno*=-1; guiLock(nextWeek)()});
    $("#searchbox").on("keyup", search).val("");
  });
  if (id = parseInt($.cookie("id"))){
    genMe();
    loadtable[loadTable](id);
    history.pushState({callback:"table", args:{viewing_week:0, loadTable:loadTable}},"","");
    return;
  }
  $("#overshadow").addClass("on").load("myid.html")
    .submit(function(){
      $("#overshadow").removeClass("on");
      $.cookie("id",id=$("#id").val());
      genMe();
      loadtable[loadTable](id);
      history.pushState({callback:"table", args:{viewing_week:0, loadTable:loadTable}},"","");
      //setTimeout(help,1000);
      return false
    });
  function genMe(){
    $(".content.personal").append($("<div>",{id:id, class:"container"}));
    genPersonalTables();
    loadPersonalTables();
  };
});

// transition utilities

function getOldUsers(data){
    var ret="";
    for (var i in data){
      var e=data[i];
      ret+=e.matricola+","+e.nome+","+(e.dieta || "no")+"\n";
    }
    return ret;
}

// tableAll

function genTableAll(people, range, presences){
  var table = $("<div>",{class:"list centered z1 inactive"});
  presences = buildPresences(presences);
  genTableAllHead(range).css({position:"absolute"}).addClass("all").appendTo(table);
  genTableAllHead(range).appendTo(table);
  for (var i in people){
    genTableAllRow(range, findPerson(people, people[i].id), presences[people[i].id]).appendTo(table);
  }
  return table;
}

function genTableAllHead(range){
  var th = $("<div>").addClass("item head");
  $("<div>").addClass("item id").text("sigla").appendTo(th).click(sortAll("id"));
  $("<div>").addClass("item name").text("nome").appendTo(th).click(sortAll("name"));
  for (var now = moment(range.from_timestamp); now>=range.from_timestamp && now<=range.to_timestamp; now.add(1,'days')){
    $("<div>").addClass("item").append($("<span>").html(now.locale('it').format('dddd[<br>]Do MMMM'))).appendTo(th).
      data("timestamp",now.valueOf()).
      click(showSummary);
  }
  return th;
}

function genTableAllRow(range, person, presences){
  var tr = $("<div>").addClass("item");
  $("<div>",{class:"item numeric id solid"}).text(person.id).appendTo(tr);
  $("<div>").addClass("item name solid").text(person.name+(person.diet=="no"?"":" *")).appendTo(tr).click(function(){
    addPersonalTable(person.id); switchView();});
  for (var now = moment(range.from_timestamp); now>=range.from_timestamp && now<=range.to_timestamp; now.add(1,'days')){
    var periods = presences? presences.presences[now.valueOf()] || null :null;
    genTableAllItem({periods:periods, id:person.id, timestam:now.valueOf()}).appendTo(tr);
  }
  return tr;
}

function genTableAllItem(periods){
  var info = periods.periods || {};
  var day = $("<div>").addClass("item day");
  $("<div>", {"data-id":periods.id, "data-timestam":periods.timestam, "data-period": "breakfast", "data-presence":info.breakfast})
    .addClass(info.breakfast+" z1").click(clickCallback).appendTo(day)
    .append($("<span>", {class:"fa fa-cutlery"})).append($("<span>", {class:"fa fa-times"})).append($("<span>", {class:"fa fa-briefcase"}));
  $("<div>", {"data-id":periods.id, "data-timestam":periods.timestam, "data-period": "lunch", "data-presence":info.lunch})
    .addClass(info.lunch+" z1").click(clickCallback).appendTo(day)
    .append($("<span>", {class:"fa fa-cutlery"})).append($("<span>", {class:"fa fa-check"})).append($("<span>", {class:"fa fa-times"})).append($("<span>", {class:"fa fa-briefcase"}));
  $("<div>",{"data-id":periods.id, "data-timestam":periods.timestam, "data-period": "dinner", "data-presence":info.dinner})
    .addClass(info.dinner+" z1").click(clickCallback).appendTo(day)
    .append($("<span>", {class:"fa fa-cutlery"})).append($("<span>", {class:"fa fa-check"})).append($("<span>", {class:"fa fa-times"})).append($("<span>", {class:"fa fa-briefcase"}));
  var active=moment();
  day.addClass((periods.timestam<active?"transparent":"solid"));
  return day;
}

var order = 1;
function sortAll(field){ return function () { order*=-1;
  var sorted = sortPeople(peopleCache).sort(function(a,b){
    return {
      "number": a[field]>b[field]?order:order*-1,
      "string": order*((a[field]+"").localeCompare(b[field]))
    }[typeof a[field]];
  });
    $(".list.centered").remove();
  $("body").append(genTableAll(sorted, rangeCache, presencesCache));
}}

// tablePersonal

var personCache = {};

function genTablePersonal(id, range, presences){
  presences = buildPresences(presences);
  var person = presences[id] || {id:id, presences:{}}; var name; var active=moment();
  var table = $("<div>",{class:"list centered z1 mobile inactive"});
  table.append($("<div>",{class:"item custom red fa fa-times"}).click(function(){$("#"+id+".custom.container").remove(); removePersonalTable(id)}));
  $("<div>",{class:"item head"}).appendTo(table)
    .append(name = $("<div>",{class:"item"}))
    .append($("<div>",{class:"item"})
      .append($("<div>",{class:"fa fa-coffee fa-2x"}))
      .append($("<div>",{class:"fa fa-sun-o fa-2x"}))
      .append($("<div>",{class:"fa fa-moon-o fa-2x"}))
    );
  for (var now = moment(range.from_timestamp); now>=range.from_timestamp && now<=range.to_timestamp; now.add(1,'days')){
    var inactive = (now<active?"transparent":"solid")
    $("<div>",{class:"item"}).appendTo(table).addClass(inactive)
      .append($("<div>").addClass("item").append($("<span>").html(now.locale('it').format('dddd[<br>]Do MMMM'))))
      .append(genTableAllItem({periods:person.presences[now.valueOf()], id:id, timestam:now.valueOf()}));
  }
  if (personCache.id!=id) $.getJSON("/rest/users/list", function(data){
    name.text((personCache=findPerson(data, id)).name);
  });
  else name.text(personCache.name);
  return table;
}

function addPersonalTable(id){
  var ptables = $.cookie("mytables"); ptables = ptables?ptables.split(","):[];
  ptables.push(id);
  $.cookie("mytables",ptables.join(","));
}

function genPersonalTables(){
  var ptables = $.cookie("mytables"); ptables = ptables?ptables.split(","):[];
  $(".content.personal .custom").remove();
  var personalTables = $(".content.personal");
  for (var i in ptables){
    personalTables.append($("<div>",{id:ptables[i], class:"custom container"}));
  }
}

function loadPersonalTables(){
  $(".custom").each(function(i,v){
    loadtablePersonal($(v).attr("id"));
  });
}

function removePersonalTable(id){
  var removed = $.cookie("mytables");
  removed = removed.replace(id+",","");
  removed = removed.replace(id+"","");
  document.cookie="mytables="+removed;
}

// summary

function summary(data, timestam){ data = data || window.data; if (!data) return;
  if (window.data) {
    $(".summary .footer").hide();
    $("body").css({"background-color":"white"});
  }
  $("#refersto").text(moment(timestam).locale("it").format("ddd Do MMM YYYY"));
  var diet_array = new Array();
  for (var i in data){
    var r = data[i];

    if (r.presence && r.total) $("."+r.period+"."+r.presence+" .total").text(r.total);
    if (r.diet_id) { $("."+r.period+" .diets").append($("<span>").text(r.diet_id)); diet_array.push(r.diet_id); }
    if (r.pocket_id) $("."+r.period+" .pocket").append($("<span>").text(r.pocket_id + (diet_array.includes(r.pocket_id) ? "*" : "")));
  }
  if (!(data && timestam)) return;
  var timestam = data[0].timestam; console.log(timestam);
  $('*[data-timestam='+timestam+'][data-period=lunch][data-presence=yes]').each(function(i,v){
    $(".lunch:not(.pocket) .summary-names").append($("<li>")
      .text(findPerson(peopleCache,$(v).data("id")).name));
  });
  $('*[data-timestam='+timestam+'][data-period=dinner][data-presence=yes]').each(function(i,v){
    $(".dinner:not(.pocket) .summary-names").append($("<li>")
      .text(findPerson(peopleCache,$(v).data("id")).name));
  });/*
  $('*[data-timestam='+timestam+'][data-period=breakfast][data-presence=pocket]').each(function(i,v){
    $(".breakfast.pocket .summary-names").append($("<li>")
      .text(findPerson(peopleCache,$(v).data("id")).name));
  });
  $('*[data-timestam='+timestam+'][data-period=lunch][data-presence=pocket]').each(function(i,v){
    $(".lunch.pocket .summary-names").append($("<li>")
      .text(findPerson(peopleCache,$(v).data("id")).name));
  });
  $('*[data-timestam='+timestam+'][data-period=dinner][data-presence=pocket]').each(function(i,v){
    $(".dinner.pocket .summary-names").append($("<li>")
      .text(findPerson(peopleCache,$(v).data("id")).name));
  });*/
}

function bareSummary(data){
  var w = open("summary.html");
  w.data = data;
}

// date range

var dateRange = {};

dateRange.viewing_week = parseInt($.cookie("viewing_week")) || 0;
dateRange.daysno = mobilecheck()?-1:1;

dateRange.get = function getDateRange(){
  if (dateRange.daysno==1 || loadTable=="personal") return {
    from_timestamp:moment().startOf('isoweek').add(dateRange.viewing_week,'weeks').startOf('day').valueOf(),
    to_timestamp:moment().endOf('isoweek').add(dateRange.viewing_week,'weeks').startOf('day').valueOf()
  }; else
  if (dateRange.daysno==-1) return {
    from_timestamp:moment().startOf('isoweek').add(dateRange.viewing_week,'days').startOf('day').valueOf(),
    to_timestamp:moment().startOf('isoweek').add(dateRange.viewing_week,'days').startOf('day').valueOf()
  };
}

// json manipulation

function sortPeople(people){
  var ret = []; for (var i in people) ret.push(people[i]);
  ret.sort(function(a,b){
    return a.name.localeCompare(b.name);
  }); return ret;
}

function findPerson(people, id){
  for (var i in people)
    if (people[i].id == id) return people[i];
}

function buildPresences(presences){
  var people = {};
  for (var i in presences){
   var presence = presences[i];
   if (!people[presence.id]) people[presence.id] = {id:presence.id, name:presence.name, presences:{}}
   var person = people[presence.id];
   if(!person.presences[presence.timestam]) person.presences[presence.timestam] = {};
   var day = person.presences[presence.timestam];
   day[presence.period] = presence.presence;
  }
  return people;
}

// sending

var send = {};

function clickCallback(e){
  var e = $(e.target).closest("*[data-id]");
  if (!toggle(e)) return;
  send.presence(e.data());
};

send.queue = [];
send.timeout = null;

send.presence = function sendPresence(data){
  send.queue.push(data);
  clearTimeout(send.timeout);
  send.timeout = setTimeout(flush, 3000);
  $("#flush").addClass("green circle z1");
}

function flush(){
  if (send.queue.length) $.ajax({
      type:"POST", url:"/rest/users/presence",
      contentType: "application/json", data: JSON.stringify(send.queue),
      success: function(){
        confirm(send.queue);
        send.queue = [];
        $("#flush").removeClass("green circle z1");
        clearTimeout(send.timeout);
      },
      error: function(){
        var retry = window.confirm("Errore nella connessione. Vuoi Ritentare?");
        if (retry) flush();
        else location = location.origin;
      }
  });
}

function toggle (e){
  if (!validate(e.data())) return;
  e.addClass("unconfirmed");
  if (e.hasClass("undefined") && e.data("period")!="breakfast"){
    e.removeClass("undefined");
    e.addClass("yes");
    e.data("presence","yes");
  }
  else if (e.hasClass("yes")){
    e.removeClass("yes");
    e.addClass("no");
    e.data("presence","no");
  }
  else if (e.hasClass("no")){
    e.removeClass("no");
    e.addClass("pocket");
    e.data("presence","pocket");
  }
  else if (e.hasClass("pocket") && e.data("period")!="breakfast"){
    e.removeClass("pocket");
    e.addClass("yes_pocket");
    e.data("presence","yes_pocket");
  } else if (e.hasClass("yes_pocket") && e.data("period")!="breakfast"){
    e.removeClass("yes_pocket");
    e.addClass("yes");
    e.data("presence","yes");
  }else if (e.hasClass("undefined") && e.data("period")=="breakfast"){
    e.removeClass("undefined");
    e.addClass("pocket");
    e.data("presence","pocket");
  } else if (e.hasClass("pocket") && e.data("period")=="breakfast"){
    e.removeClass("pocket");
    e.addClass("no");
    e.data("presence","no");
  }
  return true;
}

function confirm(list){
  for (var i in list){
    var e = list[i];
    $('*[data-id='+e.id+'][data-timestam='+e.timestam+'][data-period='+e.period+']')
      .removeClass("unconfirmed");
  }
}

function validate(presence){
  var date = new Date().valueOf();
  if (presence.timestam != (moment(presence.timestam).startOf('day')) || (presence.timestam<(date-3600000)))
    return false;
  if (presence.period=="breakfast" && ((presence.timestam-90000000)<date))
    return false;
  if (moment(presence.timestam).day()== 0 /*domenica*/ &&
  	moment(presence.timestam).startOf('day').add(-2,'days').add(23,'hours') < date )
    return false;
  return true;
}

// scroll

$(document).scroll(function(){
  var scr = $(this).scrollTop();
  $(".item.head").css({top:"calc("+scr+"px + 3em)"})
});

function mobilecheck() {
  var check = false;
  (function(a,b){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4)))check = true})(navigator.userAgent||navigator.vendor||window.opera);
  return check;
}

return exp; })();
