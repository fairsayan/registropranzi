var mysql = require('mysql');
var debug = require('debug')('mysql');

var dbConnectionsPool;

function connectDatabase(settings){
	var defaults = {
		connectionLimit : 10, //important
		debug: false,
		multipleStatements: true
	}
	for (var i in settings) defaults[i] = settings[i] || defaults[i];
	dbConnectionsPool = mysql.createPool(defaults);
	debug(defaults);
	debug(dbConnectionsPool);
}

//handle query
function handleDatabase(req, res, query, values, callback) {
	dbConnectionsPool.getConnection(function(err,connection){
        if (err) {
        	debug(err);
        	if (connection) connection.release();
        	res.status(503).end();
        } else {
			connection.query(query, values, function(err,rows,fields){
				connection.release();
				callback(req, res, err, rows, fields);
			});
		}
	});
}

module.exports = function(settings){
	connectDatabase(settings);
	return {q:handleDatabase}
}
