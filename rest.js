var db;
var validation = require('./validation.js');
var cacheFile = require('./cacheFile.js');
var confs = require('./confs.js');

var router = require('express').Router();

router.use('',function nocache(req, res, next) {
  res.header('Cache-Control', 'private, no-cache, no-store, must-revalidate');
  res.header('Expires', '-1');
  res.header('Pragma', 'no-cache');
  next();
});

router.get('/user/:id/presence', function (req, res) {
	db.q(req, res,
		"select * from presences where (id=?) and (? <= timestam) and (timestam <= ?)",
		[parseInt(req.params.id), parseInt(req.query.from_timestamp), parseInt(req.query.to_timestamp)],
		function (req, res, err, rows, fields){
			if (err) res.status(400).end();
			else res.json(rows);
		}
	);
});

router.get('/users/list', function (req, res){
	db.q(req, res, "select * from alumni", [], function(req, res, err, rows, fields){
		res.json(rows);
	});
});

router.get('/users/presence', function (req, res) {
	db.q(req, res,
		"select * from presences where (? <= timestam) and (timestam <= ?)",
		[parseInt(req.query.from_timestamp),parseInt(req.query.to_timestamp)],
		function (req, res, err, rows, fields){
			if (err) res.status(400).end();
			else res.json(rows);
		}
	);
});

function noerr(err,req,res,next){if (err instanceof SyntaxError) res.status(400).end(); else next();}

router.post('/users/presence', [require('body-parser').json(), noerr], function (req, res){
	var validated = validation(req.body);
	if (validated.errors.length || validated.customErrors.length) return res.status(400).end();
	var query = ""; for (var i in validated.instance) query+= "replace into presences set ?;"
	db.q(req, res, query, validated.instance, function (req, res, err, rows, fields){
		if (err) res.status(500).end();
		else res.json([err, rows, fields]);
	});
});

var summarypath = (process.env.DEV?"":confs.root_path)+"summary.sql";
cacheFile.add(summarypath);

router.get('/users/summary', function(req, res){
	db.q(req, res, cacheFile.files[summarypath],
		[req.query.timestamp,req.query.timestamp,req.query.timestamp,req.query.timestamp,req.query.timestamp,req.query.timestamp],
		function(req, res, err, rows, fields){
			if (err) res.status(400).json(err);
			else res.json(rows);
		}
	);
});

module.exports = function(db_conn){
	db = db_conn;
	return router;
};
