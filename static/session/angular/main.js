angular.module("registropranzi", ["ngRoute"])
  .config(["$routeProvider",function ($routeProvider){ $routeProvider
    .when("/me", {
      templateUrl: "personal.html",
      controller: "personal"
    })
  }])
  .controller("main", ["$scope", "$http", function($scope, $http){
    $http.get("/rest/users/list").then(function(response){
      $scope.users = response;
    });
  }])
