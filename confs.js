module.exports = {
  "server": {
    "ip": process.env.SERVER_IP || "www.serviziresidenza.eu",
    "port": process.env.SERVER_PORT || 8080
  },
  "db": {
    "user": process.env.DB_USER || "serviziresidenza",
    "password": process.env.DB_PASSWORD || "elis2015!",
    "host": process.env.DB_HOST || "localhost",
    "database": process.env.DB_DATABASE || "registropranzi"
  },
  "root_path": "/home/serviziresidenza/registropranzi/",
  "ssl": {
    "key": "/home/serviziresidenza/key/serviziresidenza.eu.key",
    "certificate": "/home/serviziresidenza/key/serviziresidenza_eu.crt",
    "port": 443
  }
}
