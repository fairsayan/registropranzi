var express = require('express');

var app = express();

var confs = require('./confs.js');

var db_conn = require('./db.js')(confs.db);

app.use('/', require('./session.js')(db_conn));

app.use('/rest', require('./rest.js')(db_conn));

app.use('/', express.static(__dirname + '/static/session'));

require('./mailer.js').start(db_conn);

app.use('/admin', function(req,res,next){
  if(req.session.user.permission=="admin") next(); else res.status(404).end();});
require('./admin.js')(app);

require('./ssl.js')(app);
