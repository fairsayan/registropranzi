## Configuration

confs.json

##Making

	npm init

add dependencies:

	"dependencies" : {
		"strider": "latest", //continous deployment
		"express": "latest", //main framework
	}

install dependencies

	npm install --production

install developement dependencies

	npm install

###MongoDB

install MongoDB

	mongod --smallfiles

###Strider configuration

add admin user:

	node node_modules/strider/bin/strider addUser

start strider:

	npm start
	node_modules/strider/bin/strider

[Admin Panel](localhost:3000)

add custom scripts

	npm install

	npm test

	node_modules/forever/bin/forever start server/server.js

	node_modules/forever/bin/forever start node_modules/express-admin/app.js express-admin

###NodeJS testing

	DEBUG=* node index.js
