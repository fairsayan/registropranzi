create table if not exists users (
	username varchar(32) not null, primary key (username),
  password char(32) not null,
	permission varchar(50)
);

create table if not exists alumni (
	id int unsigned not null, primary key (id),
	name varchar(255) not null,
	diet varchar(255) default 'no'
);

create table if not exists presences (
	id int unsigned not null, foreign key (id) references alumni(id),
	timestam bigint unsigned not null,
	period enum('breakfast','lunch','dinner') not null,
	presence enum('yes','no','pocket', 'yes_pocket') not null,
	primary key (id, timestam, period)
);

create table if not exists summary_mail (
	email varchar(255) not null unique
);
